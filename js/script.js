﻿var winWidth, winHeight, offset, position, width, height;

$(document).ready(function() {
	winWidth = $(window).width();
	winHeight = $(window).height();


	blockSize();

    // Прячем прелоадер при загрузке
    $('.preloader').addClass('hide').fadeOut(300);

    // Кнопка вверх
    $('.go_to_top').on('click', function() {
        $('html,body').stop().animate({'scrollTop':'0'},500);
        return false;
    });
    $(document).on('scroll', function() {
        var scrollTop = $(document).scrollTop();
        if(scrollTop > 140) {
            $('.go_to_top').addClass('visible');
            $('.header').addClass('fixed');
        } else {
            $('.go_to_top').removeClass('visible');
            $('.header').removeClass('fixed');
        }
    });

    // В моибльной версии показываем/прячем меню
    $('.mobileMenu .toggle').on('click', function() {
        $(this).parent('.mobileMenu').toggleClass('active');
        return false;
    });

    // Показываем/прячем фильтор товаров
    $('.leftPanel .filter').on('click', '.filterTitle', function() {
        $(this).parents('.filter').toggleClass('active');
        return false;
    });


    /* Расскрывающееся левое меню */
    $('.nav-service > .nav > .parent > a').on('click', function() {
        if($(this).parent('li.parent').hasClass('active')) {
            $(this).parent('li.parent').removeClass('active').find('.child').animate({ height: 0 },500).css({overflow: 'hidden'});
        } else {
            $('.nav-service > .nav > li.parent.active').removeClass('active').find('.child').animate({ height: 0 },500).css({overflow: 'hidden'});
            $(this).parent('li.parent').addClass('active').find('.child').animate({ height: $(this).parent('.parent').find('.child').attr('data-active') },500);
            $(this).parent('.child').css({overflow: 'visible'});
        }
        return false;
    });
    $('.nav-service .child > li.parent > a').on('click', function() {
        if(winWidth > 800) {
            //$(this).parent('li').toggleClass('active');
            return false;
        }
    });

    /* Мини корзина */
    $('#nav-main .basket > a, .miniBasket .close').on('click', function() {
        var popOver;
        if($(this).hasClass('close')) {
            popOver = $(this).parents('.miniBasket');
        } else {
            popOver = $(this).parent('.basket').find('.miniBasket');
        }
        if(popOver.hasClass('active')) {
            popOver.removeClass('active').fadeOut(300);
        } else {
            popOver.addClass('active').fadeIn(300);
        }
        return false;
    });

    /* Табы с информацией */
    $('.tabs .tabs-button .item').on('click', function() {
        $('.tabs .item').removeClass('active');
        $(this).addClass('active');
        $('.tabs-text .item'+$(this).attr('href')).addClass('active');
        blockSize();
        return false;
    });
    $('.tabs-text .item'+$('.tabs .tabs-button .item.active').attr('href')).addClass('active');



    /* Каталог */
    $('.catalog .parent > a').on('click', function() {
        $(this).parent('.parent').toggleClass('active');
        return false;
    });


    /* Кнопка сравнить */
    $('.btn.compare').on('click', function() {
        var currentCompare = parseInt($('.header').find('.compare span').text());
        var dataToggle;

        if($(this).parents('.item').hasClass('checked')) {
            dataToggle = $(this).attr('data-toggle');
            $(this).attr('data-toggle', $(this).text()).text(dataToggle);
            $(this).parents('.item').removeClass('checked');
            $('.header').find('.compare span').html(currentCompare - 1);
        } else {
            dataToggle = $(this).attr('data-toggle');
            $(this).attr('data-toggle', $(this).text()).text(dataToggle);
            $(this).parents('.item').addClass('checked');
            $('.header').find('.compare span').html(currentCompare + 1);
        }

        return false;
    });



    /* Продукты */
    var prod_cont = $('.product-items');
    if(prod_cont.hasClass('line')) {
        $('.product-view .btn.line').addClass('active');
        $('.right-panel').addClass('show');
    } else if(prod_cont.hasClass('grid')) {
        $('.product-view .btn.grid').addClass('active');
        $('.right-panel').addClass('hide');
    }
    $('.product-view .btn').on('click', function() {
        var view = $(this).attr('data-viewMethod');
        $('.product-view .btn').removeClass('active');
        $(this).addClass('active');
        if(view == 'grid') {
            prod_cont.removeClass('line').addClass('grid');
            $('.right-panel').removeClass('show').addClass('hide');
        } else if(view == 'line') {
            prod_cont.removeClass('grid').addClass('line');
            $('.right-panel').removeClass('hide').addClass('show');
        }
        blockSize();
        return false;
    });
    $('.leftPanel .filterBlock.accordion .title a').on('click', function() {
        if(!$(this).parents('.filterBlock').hasClass('open')) {
            $(this).parents('.filterBlock').addClass('open').removeClass('close');
        } else {
            $(this).parents('.filterBlock').addClass('close').removeClass('open');
        }
        blockSize();
        return false;
    });
        // Ползунок с ценой
    if($('#amount-range').length) {
        var amountRange = $('#amount-range');
        var amountMin = parseInt(amountRange.attr('data-min')),
            amountMax = parseInt(amountRange.attr('data-max')),
            amountStart = parseInt(amountRange.attr('data-start')),
            amountEnd = parseInt(amountRange.attr('data-end')),
            amountStep = parseInt(amountRange.attr('data-step'));
        amountRange.slider({
            range: true,
            min: amountMin,
            max: amountMax,
            values: [amountStart, amountEnd],
            step: amountStep,
            slide: function (event, ui) {
                $("#amountStart").val( ui.values[0].toString().replace(/(\d{1,3}(?=(\d{3})+(?:\.\d|\b)))/g,"\$1 ") );
                $("#amountEnd").val( ui.values[1].toString().replace(/(\d{1,3}(?=(\d{3})+(?:\.\d|\b)))/g,"\$1 ") );
            }
        });

        var amountStartElm = $("#amountStart");
        amountStartElm.val( amountRange.slider("values", 0).toString().replace(/(\d{1,3}(?=(\d{3})+(?:\.\d|\b)))/g,"\$1 ") );
        amountStartElm.on('input', function() {
            var val = $(this).val().replace(' ', '');
            if( parseInt(val) < amountMin ) {
                val = amountMin;
            }
            amountRange.slider( "values", 0, val );
            $(this).val( val.toString().replace(/(\d{1,3}(?=(\d{3})+(?:\.\d|\b)))/g, "\$1 ") );
        });
        var amountEndElm = $("#amountEnd");
        amountEndElm.val( amountRange.slider("values", 1).toString().replace(/(\d{1,3}(?=(\d{3})+(?:\.\d|\b)))/g,"\$1 ") );
        amountEndElm.on('input', function() {
            var val = $(this).val().replace(' ', '');
            if( parseInt(val) > amountMax ) {
                val = amountMax;
            }
            amountRange.slider( "values", 1, val );
            $(this).val( val.toString().replace(/(\d{1,3}(?=(\d{3})+(?:\.\d|\b)))/g,"\$1 ") );
        });
    }
        // Ползунок с весом
    if($('#weight-range').length) {
        // Ползунок с весом
        var weightRange = $('#weight-range'),
            weightMin = parseInt(weightRange.attr('data-min')),
            weightMax = parseInt(weightRange.attr('data-max')),
            weightStart = parseInt(weightRange.attr('data-start')),
            weightStep = parseInt(weightRange.attr('data-step'));
        weightRange.slider({
            range: "min",
            min: weightMin,
            max: weightMax,
            value: weightStart,
            step: weightStep,
            slide: function (event, ui) {
                $("#weightStart").val(ui.value);
            }
        });
        var weightStartElm = $("#weightStart");
        weightStartElm.val(weightRange.slider("value"));
        weightStartElm.on('input', function() {
            var val = $(this).val().replace(' ', '');
            if( parseInt(val) > weightMax ) {
                val = weightMax;
            }
            weightRange.slider( "value", val );
            $(this).val( val.toString().replace(/(\d{1,3}(?=(\d{3})+(?:\.\d|\b)))/g,"\$1 ") );
        });
    }
        // сброс параметров в ползунке
    $('.filterBlock .reset').on('click', function() {
        amountRange.slider( "values", 0, amountStart );
        amountRange.slider( "values", 1, amountEnd );
        weightRange.slider( "value", weightStart );
    });


    $('.productGallery .gallery').on('click', 'a', function() {
        var dataId = $(this).attr('data-id');
        $('.productGallery .gallery').find('a').removeClass('active');
        $('.productGallery .gallery').find('a[data-id="'+dataId+'"]').addClass('active')
        var source, sourceUrl = $(this).attr('href'), width, height, urlFull, otherData;
        if($(this).hasClass('image')) {
            source = 'image';
            urlFull = $(this).attr('data-imgfull');
        }
        if($(this).hasClass('video')) {
            source = 'video';
            width = $(this).attr('data-width');
            height = $(this).attr('data-height');
            if(!width) width = '100%';
            if(!height) height = '100%';
        }
        otherData = {
            width: width,
            height: height,
            urlFull: urlFull
        };
        productGallery(source, sourceUrl, otherData);
        return false;
    });
    var defObj = $('.productGallery .gallery').find('.active'), source, width, height, urlFull, otherData;
    if(defObj.hasClass('image')) {
        source = 'image';
        urlFull = defObj.attr('data-imgfull');
    }
    if(defObj.hasClass('video')) {
        source = 'video';
        width = defObj.attr('data-width');
        height = defObj.attr('data-height');
        if(!width) width = '100%';
        if(!height) height = '100%';
    }
    otherData = {
        width: width,
        height: height,
        urlFull: urlFull
    };
    productGallery(source, defObj.attr('href'), otherData);


    $('.nav-footer').on('click', '.parent > a', function() {
        if(winWidth <= 800) {
            $('.nav-footer .parent').removeClass('active');
            $(this).parent('.parent').addClass('active');
            return false;
        }
    });


    /* кнопка показать пароль */
    $('input[name="showPass"]').change(function() {
        if($(this).is(':checked')) {
            $(this).parents('form').find('input[type="password"]').prop('type', 'text').attr('data-type', 'password');
        } else {
            $(this).parents('form').find('input[data-type="password"]').prop('type', 'password').removeAttr('data-type');
        }
    });


    /* Оформление заказа */
    var delivery = $('.contentPlace.cabinet .order-settings .delivery input:checked').val();
    $('.contentPlace.cabinet .order-settings .address#'+delivery).addClass('active');
    $('.contentPlace.cabinet .order-settings .delivery input').change(function() {
        if($(this).is(':checked')) {
            $('.contentPlace.cabinet .order-settings .address').removeClass('active');
            $('.contentPlace.cabinet .order-settings .address#'+$(this).val()).addClass('active');
        }
        blockSize();
    });


    /* Изменение кол-во товаров в корзине */
    $('.count').on('click', '.up', function() {
        var count = $(this).parents('.count').find('input');
        count.val( parseInt(count.val()) + 1 );
        return false
    });
    $('.count').on('click', '.down', function() {
        var count = $(this).parents('.count').find('input');
        if(parseInt(count.val()) != 1) {
            count.val(parseInt(count.val()) - 1);
        }
        return false
    });






    /* POP-UP */
    $(document).on('click', '.pop-up-open', function() {
        popUpOpen('#'+$(this).attr('data-popup'));
        return false;
    });
    $('.pop-up-close').on('click', function() {
        popUpClose('#'+$(this).parents('.pop-up').attr('id'));
        if($(this).parents('.pop-up').hasClass('fullImg') && $(this).parents('.pop-up').find('.main-image').hasClass('video')) {
            $(this).parents('.pop-up').find('.main-image').html('');
        }
        return false;
    });

    $('.pop-up-rules').on('click', function() {
        $(this).parents('.pop-up').find('.rules').addClass('active');
        return false;
    });
    $('.rules-close').on('click', function() {
        $(this).parents('.pop-up').find('.rules').removeClass('active');
        return false;
    });

    $('.get-sms-pass').on('click', function() {
        alert('Это окошко только для примера, а по идеи должен уйти запрос на сервер и должно отправиться СМС');
        return false;
    });
    /* === / === */



	/* OWL Carousel (responsive & touch support) */
	$('#slider').owlCarousel({
		items : 1,
		slideSpeed : 500,
		autoPlay : 4500,
		stopOnHover : true,
		navigation : false,
		navigationText : false,
		pagination : true,
		itemsDesktop : [1199,1],
		itemsDesktopSmall : [1024,1],
		itemsTablet : [800,1],
		itemsMobile : [500,1]
	});
    $('#new-arrivals, #sale, #watched').owlCarousel({
        items : 5,
        slideSpeed : 500,
        autoPlay : 4500,
        stopOnHover : true,
        navigation : true,
        navigationText : false,
        pagination : false,
        itemsDesktop : [1400,4],
        itemsDesktopSmall : [1200,3],
        itemsTablet : [800,2],
        itemsMobile : [470,1]
    });
    $('#compare-list').owlCarousel({
        items : 5,
        slideSpeed : 500,
        autoPlay : false,
        navigation : true,
        navigationText : false,
        pagination : false,
        itemsDesktop : [1400,4],
        itemsDesktopSmall : [1200,3],
        itemsTablet : [800,2],
        itemsMobile : [470,1],
        afterUpdate : function() {
            var currentNum = $(this);
            $('.compare-list-num .view').text(currentNum[0].options.items);
            $('.compare-list .compare-group-title').css({ 'top': ($('.compare-list .item .prod').height() + 104) });
        }
    });
    $('#serviceCenter').owlCarousel({
        items : 8,
        slideSpeed : 500,
        autoPlay : false,
        stopOnHover : true,
        navigation : true,
        navigationText : false,
        pagination : false,
        itemsDesktop : [1400,6],
        itemsDesktopSmall : [1200,5],
        itemsTablet : [1100,4],
        itemsMobile : [600,2]
    });
    $('#main-information').owlCarousel({
        items : 7,
        slideSpeed : 500,
        autoPlay : false,
        stopOnHover : true,
        navigation : true,
        navigationText : false,
        pagination : false,
        itemsDesktop : [1400,5],
        itemsDesktopSmall : [1200,4],
        itemsTablet : [800,3],
        itemsMobile : [450,2]
    });
    $('#brands').owlCarousel({
        items : 7,
        slideSpeed : 500,
        autoPlay : false,
        stopOnHover : true,
        navigation : true,
        navigationText : false,
        pagination : false,
        itemsDesktop : [1200,5],
        itemsDesktopSmall : [1024,4],
        itemsTablet : [800,3],
        itemsMobile : [500,2]
    });
    $('#gallery, #fullGallery').owlCarousel({
        items : 5,
        slideSpeed : 500,
        autoPlay : false,
        stopOnHover : true,
        navigation : true,
        navigationText : false,
        pagination : false,
        itemsDesktop : [1300,5],
        itemsDesktopSmall : [1200,4],
        itemsTablet : [1024,3],
        itemsMobile : [860,2]
    });
    $('#advantage').owlCarousel({
        items : 5,
        slideSpeed : 500,
        autoPlay : 4500,
        stopOnHover : true,
        navigation : true,
        navigationText : false,
        pagination : true,
        itemsDesktop : [1400,4],
        itemsDesktopSmall : [1200,3],
        itemsTablet : [600,2],
        itemsMobile : [470,2]
    });
	/* === / === */


    /* compare page */
    if($('.contentPlace').hasClass('compare')) {
        $('#leftPanel .leftContacts').css({ 'margin-top': ($('.compare-prod').height() + 115) - $('.nav-service .parent.active .child').height() });
        if(winWidth > 1200) {
            var topOffset = '62';
        } else {
            var topOffset = '84';
        }
        $('.compare-list .compare-group-title').css({ 'top': ($('.compare-list .item .prod').height() + topOffset) });

        $('.paramHover').hover(
            function() { if(winWidth > 900) { $('.paramHover[data-hover="'+$(this).attr('data-hover')+'"]').addClass('hover'); } },
            function() { if(winWidth > 900) { $('.paramHover').removeClass('hover'); } }
        );
        $('.paramHover').on('click', function() {
            if(winWidth <= 899) {
                $('.paramHover[data-hover="'+$(this).attr('data-hover')+'"]').toggleClass('hover');
            }
        });

        comparePageTitleWidth();

        var compareList = $('#compare-list').data('owlCarousel');
        $('.compare-list-num .view').text(compareList.options.items);

        $('.compare-list .compare-group-title .group').on('click', function () {
            if(winWidth <= 899) {
                $(this).toggleClass('active');
            }
        });

        comparePageMiniProd('');
        var itemHeight = $('.compare-prod .item .prod').height();
        $(document).on('scroll', function() {
            var scrollTop = $(this).scrollTop();
            var scrollStart = $('.compare-prod').offset();
            var miniTopOffset = scrollTop - (scrollStart.top - ($('.compare-prod .item').find('.prodMini').height() + 10));
            if(scrollTop > ((scrollStart.top + itemHeight) - 155)) { // minimum 135
                comparePageMiniProd('show');
                $('.compare-prod .item').find('.prodMini').css({top: miniTopOffset});
            } else {
                comparePageMiniProd('hide');
            }
        });
    }

});

function comparePageMiniProd(toggle) {
    var prodItem = $('.compare-prod .item');
    var miniProd = prodItem.find('.prodMini');
    if(!miniProd.length || miniProd.length <= 0) {
        prodItem.each(function() {
            var html = '<div class="prodMini hide"></div>';
            $(this).prepend(html);
            $(this).find('.prod .title').clone().appendTo( $(this).find('.prodMini') );
            $(this).find('.prod .rating').clone().appendTo( $(this).find('.prodMini') );
            $(this).find('.prod .price').clone().appendTo( $(this).find('.prodMini') );
        });
    }
    if(toggle && !miniProd.hasClass(toggle)) miniProd.removeClass('hide show').addClass(toggle);
}


function comparePageTitleWidth() {
    var prodTitleWidth;
    if( ($('.compare-prod .item').length * $('.compare-prod .item').width()) > $('.compare-list').width() ) {
        prodTitleWidth = $('.compare-list').width();
    } else {
        prodTitleWidth = ($('.compare-prod .item').length * $('.compare-prod .item').width());
    }
    $('.compare-group-title .prod-title').width(prodTitleWidth);

    $('.compare-list .compare-group-title').css({ 'top': ($('.compare-list .item .prod').height() + 104) });
}

function productGallery(object, url, data) {
    if(object == 'image') {
        $('.main-info .productGallery .main-image').html('<img src="'+url+'" /><a href="'+data.urlFull+'" class="zoom pop-up-open" data-popup="pop-up-fullImg" target="_blank"></a>');
        $('.pop-up.fullImg .productGallery .main-image').removeClass('video').addClass('image').html('<img src="'+data.urlFull+'" /></a>');
    } else if(object == 'video') {
        $('.main-info .productGallery .main-image').html('<iframe width="'+data.width+'" height="'+data.height+'" src="'+url+'" frameborder="0" allowfullscreen></iframe>');
        $('.pop-up.fullImg .main-image').removeClass('image').addClass('video').html('<iframe src="'+url+'" frameborder="0" allowfullscreen></iframe>');
    }
    popUpSize();
}

function popUpOpen(id) {
    $(document).find('.pop-up').removeClass('active');
    $(document).find(id).addClass('active');
    $('.mobileMenu').removeClass('active');
    popUpSize();
}
function popUpSize() {
    $(document).find('.pop-up.active .pop-up-body').removeAttr('style');
    $(document).find('.pop-up.active .pop-up-cont').removeAttr('style');
    var bodyHeight = $(document).find('.pop-up.active .pop-up-body').height(),
        contHeight = $(document).find('.pop-up.active .pop-up-cont').height();
    if(contHeight > bodyHeight) {
        $(document).find('.pop-up.active .pop-up-body').css({paddingRight: 20});
        $(document).find('.pop-up.active .pop-up-cont').css({height: bodyHeight, paddingRight: 10});
    }
}
function popUpClose(id) {
    $(document).find(id).removeClass('active');
    $(document).find(id + ' .pop-up-body').removeAttr('style');
    $(document).find(id + ' .pop-up-cont').removeAttr('style');
}


function setEqualHeight(columns) {
    var tallestcolumn = 0;
    columns.each(function() {
        var currentHeight = $(this).height();
        if(currentHeight > tallestcolumn) {
            tallestcolumn = currentHeight;
        }
    });
    columns.height(tallestcolumn);
}



/* Exhibiting block sizes and margins */
function blockSize() {
	winWidth = $(window).width();
	winHeight = $(window).height();

    if($(document).find('.setEqualHeight').length) {
        setTimeout(function() {
            $(document).find('.setEqualHeight').addClass('resized').each(function () {
                $(this).find('.item').height('auto');
                setEqualHeight($(this).find('.item'));
            });
        }, 500);
    }

    if(winHeight > $(document).height()) {
        $('.go_to_top').hide();
    } else {
        $('.go_to_top').show();
    }

    var rightPanel = $('#rightPanel'),
        leftPanel = $('#leftPanel');

    if(winWidth >= 1600) {
        $('.last-news').css({'max-height': $('.slider').height()});
    }

    /* Ширина контентной панельки */
    //if(winWidth >= 800) rightPanel.width( winWidth - leftPanel.width() );

    /* Выравнивание высоты левой и правой (контентной) панельки */
    if(winWidth >= 779) {
        leftPanel.height(0);
        var leftHeight = leftPanel.height(),
            rightHeight = (rightPanel.height());
        if (leftHeight <= rightHeight) {
            leftPanel.height(rightHeight);
        }
    } else {
        leftPanel.height('auto');
    }

    /* Находим высоту блоков левого подменю и записываем */
    $('.nav-service .parent').each(function() {
        var elm = $(this).find('.child');
            elm.removeAttr('style');
        var elmHeight = elm.height();
            elm.attr('data-active', elmHeight);
        if(!$(this).hasClass('active')) { elm.height(0); } else { elm.height(elmHeight); }
    });

    /* Перемещение эллементов */
    if(winWidth <= 799) {
        var container = $(document).find('.contentPlace.products .product-items');
        if(!container.hasClass('grid')) {
            container.removeClass('line').addClass('grid');
            container.parents('.contentPlace.products').find('.product-view .btn').removeClass('active');
            container.parents('.contentPlace.products').find('.product-view .btn.grid').addClass('active');
        }
    }
    popUpSize();

    comparePageTitleWidth();
}
/* === / === */


$(window).load(function() {
	blockSize();
});
$(window).on('resize', function() {
	blockSize();
});