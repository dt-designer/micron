<h1>Интернет магазин "Микрон"</h1>
<p>Адаптивная верстка и скрипты на jquery</p>
<h3>Список сверстаных страниц:</h3>
<ul>
    <li><a href="http://test.dt-designer.ru/micron/index.html">Главная</a> http://test.dt-designer.ru/micron/index.html</li>
    <li><a href="http://test.dt-designer.ru/micron/catalog.html">Каталог категорий</a> http://test.dt-designer.ru/micron/catalog.html</li>
    <li><a href="http://test.dt-designer.ru/micron/products.html">Каталог товаров</a> http://test.dt-designer.ru/micron/products.html</li>
    <li><a href="http://test.dt-designer.ru/micron/product-info.html">Карточка товара</a> http://test.dt-designer.ru/micron/product-info.html</li>
    <li><a href="http://test.dt-designer.ru/micron/compare-empty.html">Страница сравнения товаров (пусто)</a> http://test.dt-designer.ru/micron/compare-empty.html</li>
    <li><a href="http://test.dt-designer.ru/micron/compare.html">Страница сравнения товаров</a> http://test.dt-designer.ru/micron/compare.html</li>
    <li><a href="http://test.dt-designer.ru/micron/basket-empty.html">Корзина (пусто)</a> http://test.dt-designer.ru/micron/basket-empty.html</li>
    <li><a href="http://test.dt-designer.ru/micron/basket.html">Корзина</a> http://test.dt-designer.ru/micron/basket.html</li>
    <li><a href="http://test.dt-designer.ru/micron/order.html">Заказ товара</a> http://test.dt-designer.ru/micron/order.html</li>
    <li><a href="http://test.dt-designer.ru/micron/cabinet.html">Кабинет пользователя</a> http://test.dt-designer.ru/micron/cabinet.html</li>
    <li><a href="http://test.dt-designer.ru/micron/service.html">Услуги по ремонту</a> http://test.dt-designer.ru/micron/service.html</li>
    <li><a href="http://test.dt-designer.ru/micron/service-notebook.html">Страница описания услуг по ремонту</a> http://test.dt-designer.ru/micron/service-notebook.html</li>
    <li><a href="http://test.dt-designer.ru/micron/contacts.html">Контакты</a> http://test.dt-designer.ru/micron/contacts.html</li>
    <li><a href="http://test.dt-designer.ru/micron/payment-delivery.html">Как опалатить (пример текстовой страницы)</a> http://test.dt-designer.ru/micron/payment-delivery.html</li>
    <li><a href="http://test.dt-designer.ru/micron/404.html">Ошибка 404</a> http://test.dt-designer.ru/micron/404.html</li>
    <li><a href="http://test.dt-designer.ru/micron/empty.html">Пустой макет</a> http://test.dt-designer.ru/micron/empty.html</li>
</ul>